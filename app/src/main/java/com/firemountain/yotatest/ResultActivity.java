package com.firemountain.yotatest;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ResultActivity extends AppCompatActivity {

    SwipeRefreshLayout mySwipeRefreshLayout;
    TextView resultView;
    String resultText = null;
    String siteName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        mySwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.my_swipe_refresh_layout);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);

        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();

        if (ab != null)
            ab.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        siteName = intent.getStringExtra("SITE_NAME");

        if (savedInstanceState != null)
            resultText = savedInstanceState.getString("SAVED_HTML");


        if(resultText == null)
            new ProgressTask().execute();


        resultView = (TextView) findViewById(R.id.result);

        if(resultText != null)
            findViewById(R.id.loading_panel).setVisibility(View.GONE);
            resultView.setText(resultText);

        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {

                    @Override
                    public void onRefresh() {
                        new ProgressTask().execute();
                        mySwipeRefreshLayout.setRefreshing(false);
                    }
                }
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return false;
    }

    class ProgressTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... path) {
            String content;

            try{
                content = getContent(siteName);
                ResultActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(ResultActivity.this, "Данные загружены", Toast.LENGTH_SHORT)
                                .show();
                    }
                });
            }

            catch (IOException ex){
                content = null;

                ResultActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        AlertDialog.Builder builder =
                                new AlertDialog.Builder(ResultActivity.this);
                        builder.setTitle("Ошибка!");
                        builder.setMessage("Отсутствует подключение к интернету или введен несуществующий адрес");
                        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                findViewById(R.id.loading_panel).setVisibility(View.VISIBLE);

                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        new ProgressTask().execute();
                                    }
                                }, 5000);
                            }
                        });

                        if(!ResultActivity.this.isFinishing())
                            builder.show();
                    }
                });
            }
            return content;
        }

        @Override
        protected void onProgressUpdate(Void... items) {
        }

        @Override
        protected void onPostExecute(String content) {
            resultText = content;
            resultView.setText(content);
            findViewById(R.id.loading_panel).setVisibility(View.GONE);
        }

        private String getContent(String path) throws IOException {

            BufferedReader reader = null;

            URL url = new URL(path);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            try {
                urlConnection.setRequestMethod("GET");
                urlConnection.setReadTimeout(10000);
                urlConnection.connect();

                reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                StringBuilder buf = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                   buf.append(line).append("\n");
                }

                return(buf.toString());

            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e){
                        e.printStackTrace();
                    }
                }
                urlConnection.disconnect();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putString("SAVED_HTML", resultText);
    }
}
